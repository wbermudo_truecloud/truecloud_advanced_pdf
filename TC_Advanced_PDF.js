if (typeof (TC) == 'undefined') {
    TC = {
        __namespace : true
    };
}
 
/*
 * @version 1.0 - Initial Released
 * @author - wbermudo@truecloud.com
 */
TC.EVT = {
    /*
     * This is the beforeLoad script that generates the button on the transaction form
     */
    beforeLoad: function(type, form, request) {
        var funcName = 'EVT.beforeLoad';
         
        try {
            var id         = nlapiGetRecordId()
            ,   recordtype = nlapiGetRecordType();
            if (type == 'view') {
                var url = nlapiResolveURL('SUITELET', 'customscript_tc_sl_gen_pdf_report', 'customdeploy_tc_sl_gen_pdf_report')
                url += '&rectype='+recordtype;
                url += '&recid='+id;
                url += '&output=pdf';
                 
                switch (recordtype) {
                    case 'salesorder':
                        form.addButton('custpage_btn_generate_pdf', 'Generate Report', 'window.open(\'' + url + '\')');
                    break;
                }
            }
        } catch (e) {
            (e instanceof nlobjError) ? nlapiLogExecution('ERROR', 'System Error', e.getCode() + '<br/>' + e.getDetails()) : 
                                        nlapiLogExecution('ERROR', 'Unexpected Error', e.toString());
        }
    },
    
    main: function(request, response) {
        var funcName = 'EVT.main';
        
        try {
            var id      = request.getParameter('recid')
            ,   rectype = request.getParameter('rectype')
            ,   output  = request.getParameter('output')
            ,   type    = request.getParameter('type');
            nlapiLogExecution('DEBUG', funcName, 'id='+id+',rectype='+rectype+',output='+output);
             
            if (!id || !rectype || !output) {
                throw nlapiCreateError('9999', 'Record is not provided or the Output type of the PDF');
            }
            
            switch (rectype) {
                case 'salesorder':
                    this.IO.salesorder(rectype, id, output, type);
                break;
            }
        } catch(e) {
            (e instanceof nlobjError) ? nlapiLogExecution('ERROR', 'System Error', e.getCode() + '<br/>' + e.getDetails()) : 
                                        nlapiLogExecution('ERROR', 'Unexpected Error', e.toString());
        }
    },
    
    IO: {
        salesorder: function(rectype, recid, output, type) {
            var funcName = 'IO.salesorder';
            
            var outXML = null
            ,   outPDF = null;
            switch (type) {
                case 'pdfset':
                    var template = TC.EVT.__CONST.XML.pdfset;
                    if (template) {
                        var xmltemplate = TC.EVT.PDF.render(template);
                        if (xmltemplate) {
                            var renderer = nlapiCreateTemplateRenderer();           
                            renderer.setTemplate(xmltemplate);
                            outXML = renderer.renderToString();
                            outPDF = TC.EVT.PDF.print(outXML);
                        }
                    }
                break;
                
                default:
                    var template = TC.EVT.__CONST.XML.salesorder;
                    if (template) {
                        var thisRec     = nlapiLoadRecord(rectype, recid)
                        ,   location    = thisRec.getFieldValue('location')
                        ,   locationRec = null;
                        if (location) {
                            locationRec = nlapiLoadRecord('location', location);
                        }
                        
                        //TC.EVT.PDF.initValues.NLENTITYID = customerRec.getFieldValue('memo');
                        var xmltemplate = TC.EVT.PDF.render(template);
                        
                        if (xmltemplate) {
                            var renderer = nlapiCreateTemplateRenderer();           
                            renderer.setTemplate(xmltemplate);
                            renderer.addRecord('salesorder', thisRec);
                            renderer.addRecord('location', locationRec);
                            
                            outXML = renderer.renderToString();
                            outPDF = TC.EVT.PDF.print(outXML);
                        }
                    }
                break;
            }
            
            switch (output) {
                case 'xml':
                    response.setContentType('XMLDOC', ['report-', rectype+'-'+recid, '.xml'].join(''), 'inline');
                    response.write(outXML);
                break;
                 
                default:
                    response.setContentType('PDF', ['report-', rectype+'-'+recid, '.pdf'].join(''), 'inline');
                    response.write(outPDF.getValue());   
                break;
            }
        }
    },
    
    PDF: {
        render: function(id)
        {
            var xml = null;
            if (id) {
                xml = nlapiLoadFile(id).getValue();
                xml = this.initialize(xml);
            }
            return xml;
        }
         
    ,   initialize: function(xml)
        {
            for (var key in this.initValues) {
                var value = this.initValues[key]
                ,   rx    = new RegExp('\\${' + key + '}', 'g');
                xml = xml.replace(rx, value || '');
            }
            return xml;
        }
         
    ,   print: function(xml)
        {
            return nlapiXMLToPDF(xml);
        }
         
    ,   initValues:
        {
            'NLDATEPRINTED':    nlapiDateToString(new Date())
        ,   'NLUSERNAME':       'William Bermudo' //nlapiGetUser()
        }
    },
    
    __CONST: {
        XML: {
            salesorder: nlapiGetContext().getSetting('SCRIPT', 'custscript_tc_so_xml')
        ,   pdfset:     nlapiGetContext().getSetting('SCRIPT', 'custscript_tc_so_xml_pdfset')
        }
    ,   ATTACHMENT: {
            folder: '3284'    
        }
    },
    
     __namespace : true
}